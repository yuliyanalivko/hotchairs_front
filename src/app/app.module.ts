import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatSliderModule } from '@angular/material/slider';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
@NgModule({
  declarations: [
    AppComponent
  ],
	imports: [
		BrowserModule,
		MatSliderModule,
		AppRoutingModule,
		NoopAnimationsModule,
		MatFormFieldModule,
		MatDatepickerModule,
		MatInputModule,
		MatIconModule
	],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
